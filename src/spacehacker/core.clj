(ns spacehacker.core (:gen-class))
(import [java.net URL])
(require '(clojure [string :as string]))
(require '(net.cgrand [enlive-html :as html]))
(use 'incanter.core)
(use 'incanter.io)
(use 'clojure.pprint)

;; ---- UTILS ----

(defn fetch
  "Take url and return html object."
  [url]
  (html/html-resource (URL. url)))

(defn to-keyword
  "Take string and return normalized keyword."
  [input]
  (-> input
      string/lower-case
      (string/replace \space \-)
      keyword))

(defn make-counter "Returns a counter"
  [i]
  (let [c (atom i)] #(swap! c inc)))

;; ---- INIT ----

(def *counter (make-counter 0))
(def *progress (make-counter 0))
(def ^:dynamic *total* 1239)

(def *firstpage-url "https://wiki.hackerspaces.org/w/index.php?title=Special:Ask&limit=500&offset=0&q=%5B%5BCategory%3AHackerspace%5D%5D%5B%5Bhackerspace+status%3A%3Aactive%5D%5D&p=format%3Dbroadtable%2Fmainlabel%3Dhackerspace&sort=Last+Updated&order=descending#")

;; ---- PROFILE PROCESSING ----

(defn save-profile
  "Save profile to output directory."
  [profile]
  (let [filename (str "output/profile" (*counter) ".csv")]
    (save profile filename)))

(defn profile
  "Take url, parse it, then save and return profile dataset."
  [url]
  (let [content (html/select (fetch url) [:div#mw-content-text])
        table (html/select content [[:table html/first-of-type]])
        items (->> (html/select table [:tr])
                   (map html/text)
                   (map #(string/replace % "\n" " "))
                   (map #(string/replace % "," " "))
                   (map #(string/trim %))
                   ; DEBUG
                   ; (map #(string/replace % % (str "^" % "$")))
                   (filter #(re-find #"  " %)))
        header (map to-keyword (map #(second (re-find #"(.*?)(\ \ )(.*)" %)) items))
        row (map #(string/trim (last (re-find #"(.*?)(\ \ )(.*)" %))) items)
        current (*progress)
        percent (int (* (/ current *total*) 100))
        profile (dataset header [row])]
    (do (println current "/" *total* percent "%")
        (println "Parse" url)
        (save-profile profile)
        profile)))

(defn walk-profiles
  "Walk through profile links and return profile info."
  [urls]
  (do (println "Hit walk-profiles function.")
      (map profile urls)))

;; ---- PAGE PROCESSING ----

(defn next-page-url
  "Return link to next page."
  [url]
  (let [limit (last (re-find #"limit=(.*?)&" url))
        offset (last (re-find #"offset=(.*?)&" url))
        new-offset (+ (bigdec offset) (bigdec limit))]
    (string/replace url (str "offset=" offset) (str "offset=" new-offset))))

(defn lastpage?
  "True if last page of pagination"
  [html]
  (not (empty? (apply filter [#(re-find #"No results." %) (html/texts html)]))))

(defn profile-urls
  "Extract profile links from results page."
  [html]
  (let [urls (map #(string/replace-first % "/" "https://wiki.hackerspaces.org/")
                  (mapcat #(html/attr-values % :href)
                          (html/select html [:td.hackerspace :a])))]
        urls))
  
(defn walk-pages
  "Walk paginated pages."
  [url]
  (let [html (fetch url)]
        (if (lastpage? html)
          nil
          (concat (profile-urls html) (walk-pages (next-page-url url))))))

;; ---- STATISTICS ----

(defn set-total!
  "Set total number of URLs to crawl and return the urls."
  [urls]
  (do (set! *total* (length urls))
      urls))

(defn count-irc
  "Display statistics about IRC based on the datasets."
  [datasets]
  (let [all (length datasets)
        irc (length (filter #(first ($ :irc %)) datasets))
        facebook (length (filter #(first ($ :facebook %)) datasets))
        irc-ratio (int (* (/ irc all) 100))
        facebook-ratio (int (* (/ facebook all) 100))
        facebook-to-irc-ratio (int (* (/ irc facebook) 100))]
    (do (println "Total: " all)
        (println "IRC: " irc)
        (println "Facebook: " facebook)
        (println "IRC ratio: " irc-ratio "%")
        (println "Facebook ratio: " facebook-ratio "%")
        (println "Facebook to IRC ratio: " facebook-to-irc-ratio "%"))))

;; ---- MAIN ----

(defn -main []
  (count-irc (walk-profiles (walk-pages *firstpage-url))))
