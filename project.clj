(defproject spacehacker "0.1.0"
  :description "Scraper for Hackerspaces.org"
  :url "http://example.com/FIXME"
  :license {:name "GNU Affero General Public License"
            :url "http://www.gnu.org/licenses/agpl.txt"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [incanter/incanter-core "1.5.7"]
                 [incanter/incanter-io "1.5.7"]
                 [enlive "1.1.6"]]
  :main spacehacker.core
  :aot :all
  :auto-clean false
  )
