# spacehacker

Scraper for Hackerspaces.org.

## Usage

Currently it is a functional prototype that counts the ratio of IRC use based on contact information in hackerspace profiles.

## License

Copyright © 2016 maxigas

Distributed under the GNU Affero General Public License 3 or (at your option) any later version.
